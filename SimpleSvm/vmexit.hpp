#pragma once
#include "SimpleSvm.hpp"


UINT64 MapPageVirtual(UINT64 pid_local, UINT64 virtaddr_local, UINT64 pid_remote, UINT64 virtaddr_remote, PSHARED_VIRTUAL_PROCESSOR_DATA SharedVpData);

VOID SvHandleVmmcall(PVIRTUAL_PROCESSOR_DATA VpData, PGUEST_CONTEXT GuestContext);

VOID SvHandleCpuid(PVIRTUAL_PROCESSOR_DATA VpData, PGUEST_CONTEXT GuestContext);

VOID SvHandleMsrAccess(PVIRTUAL_PROCESSOR_DATA VpData, PGUEST_CONTEXT GuestContext);

VOID SvHandleVmInstruction(PVIRTUAL_PROCESSOR_DATA VpData, PGUEST_CONTEXT GuestContext);
