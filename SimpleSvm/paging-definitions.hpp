#pragma once

#include <basetsd.h>

typedef union _virt_addr_t
{
    UINT64 Value;
    struct
    {
        UINT64  offset : 12;
        UINT64  pt_index : 9;
        UINT64  pd_index : 9;
        UINT64  pdpt_index : 9;
        UINT64  pml4_index : 9;
        UINT64  reserved : 16;
    }Fields;
} virt_addr_t, * pvirt_addr_t;
#define as_virt_addr_t(addr) (reinterpret_cast<virt_addr_t>(addr))


typedef struct
{
    union {
        UINT64 AsUInt64;
        struct {
            UINT64 Valid : 1;
            UINT64 Write : 1;
            UINT64 User : 1;
            UINT64 WriteThrough : 1;
            UINT64 CacheDisable : 1;
            UINT64 Accessed : 1;
            UINT64 Ignored3 : 1;
            UINT64 LargePage : 1; // must be 0
            UINT64 Ignored2 : 4;
            UINT64 PageFrameNumber : 28;
            UINT64 Reserved1 : 12; // must be 0
            UINT64 Ignored1 : 11;
            UINT64 NoExecute : 1;
        }Fields;
    };
}PML4E, * PPML4E;
static_assert(sizeof(PML4E) == 8);

struct PageDirPointerTablePageDirEntry
{
    union
    {
        UINT64 AsUInt64;
        struct MyStruct
        {

            UINT64 Valid : 1;
            UINT64 Write : 1;
            UINT64 User : 1;
            UINT64 WriteThrough : 1;
            UINT64 CacheDisable : 1;
            UINT64 Accessed : 1;
            UINT64 Ignored3 : 1;
            UINT64 LargePage : 1; // 0 means page directory mapped
            UINT64 Ignored2 : 4;
            UINT64 PageFrameNumber : 28;
            UINT64 Reserved1 : 12; // must be 0
            UINT64 Ignored1 : 11;
            UINT64 NoExecute : 1;
        }Fields;
    };
};

struct PageDirPointerTablePageEntry
{
    union
    {
        UINT64 AsUInt64;
        struct {
            UINT64 Valid : 1;
            UINT64 Write : 1;
            UINT64 User : 1;
            UINT64 WriteThrough : 1;
            UINT64 CacheDisable : 1;
            UINT64 Accessed : 1;
            UINT64 dirty : 1;
            UINT64 LargePage : 1; // 1 means 1gb page mapped
            UINT64 global : 1;
            UINT64 Ignored2 : 3;
            UINT64 pat : 1;
            UINT64 Reserved2 : 17; // must be 0
            UINT64 PageFrameNumber : 10;
            UINT64 Reserved1 : 12; // must be 0
            UINT64 Ignored1 : 11;
            UINT64 NoExecute : 1;
        }Fields;
    };
};

typedef union
{
    struct PageDirPointerTablePageDirEntry as_dir;
    struct PageDirPointerTablePageEntry as_page; //For 1GB large pages
}  PDPE, * PPDPE;

struct PageDirPageTableEntry
{
    union
    {
        UINT64 AsUInt64;
        struct
        {
            UINT64 Valid : 1;
            UINT64 Write : 1;
            UINT64 User : 1;
            UINT64 WriteThrough : 1;
            UINT64 CacheDisable : 1;
            UINT64 Accessed : 1;
            UINT64 Ignored3 : 1;
            UINT64 LargePage : 1; // 0 means page table mapped
            UINT64 Ignored2 : 4;
            UINT64 PageFrameNumber : 28;
            UINT64 Reserved1 : 12; // must be 0
            UINT64 Ignored1 : 11;
            UINT64 NoExecute : 1;
        }Fields;
    };
};

struct PageDirPageEntry
{
    union
    {
        UINT64 AsUInt64;
        struct {
            UINT64 Valid : 1;
            UINT64 Write : 1;
            UINT64 User : 1;
            UINT64 WriteThrough : 1;
            UINT64 CacheDisable : 1;
            UINT64 Accessed : 1;
            UINT64 dirty : 1;
            UINT64 LargePage : 1;  // 1 means 2mb page mapped
            UINT64 global : 1;
            UINT64 Ignored2 : 3;
            UINT64 pat : 1;
            UINT64 Reserved2 : 8; // must be 0
            UINT64 PageFrameNumber : 19;
            UINT64 Reserved1 : 12; // must be 0
            UINT64 Ignored1 : 11;
            UINT64 NoExecute : 1;
        }Fields;
    };
};

typedef union
{
    struct PageDirPageTableEntry as_dir;
    struct PageDirPageEntry as_page; //for 2MB large pages
}  PDE, * PPDE;

typedef struct
{
    union {
        UINT64 AsUInt64;
        struct {
            UINT64 Valid : 1;
            UINT64 Write : 1;
            UINT64 User : 1;
            UINT64 WriteThrough : 1;
            UINT64 CacheDisable : 1;
            UINT64 Accessed : 1;
            UINT64 dirty : 1;
            UINT64 LargePage : 1;
            UINT64 global : 1;
            UINT64 Ignored2 : 3;
            UINT64 PageFrameNumber : 28;
            UINT64 Reserved1 : 12; // must be 0
            UINT64 Ignored1 : 11;
            UINT64 NoExecute : 1;
        }Fields;
    };
}  PTE, * PPTE;
