#include "SimpleSvm.hpp"
#include "npt.hpp"
#include "nt_utils.hpp"
#include "vmexit.hpp"


/*!
    @brief          Injects #GP with 0 of error code.

    @param[in,out]  VpData - Per processor data.
 */
_IRQL_requires_same_
static
VOID
SvInjectGeneralProtectionException(
    _Inout_ PVIRTUAL_PROCESSOR_DATA VpData
)
{
    EVENTINJ event;

    //
    // Inject #GP(vector = 13, type = 3 = exception) with a valid error code.
    // An error code are always zero. See "#GP-General-Protection Exception
    // (Vector 13)" for details about the error code.
    //
    event.AsUInt64 = 0;
    event.Fields.Vector = 13;
    event.Fields.Type = 3;
    event.Fields.ErrorCodeValid = 1;
    event.Fields.Valid = 1;
    VpData->GuestVmcb.ControlArea.EventInj = event.AsUInt64;
}


/*!
    @brief          Injects #UD with 0 of error code.

    @param[in,out]  VpData - Per processor data.
 */
_IRQL_requires_same_
static
VOID
SvInjectInvalidOpcodeException(
    _Inout_ PVIRTUAL_PROCESSOR_DATA VpData
)
{
    EVENTINJ event;

    //
    // Inject #UD(vector = 13, type = 3 = exception) with a valid error code.
    // An error code are always zero. See "#GP-General-Protection Exception
    // (Vector 13)" for details about the error code.
    //
    event.AsUInt64 = 0;
    event.Fields.Vector = 6;
    event.Fields.Type = 3;
    event.Fields.ErrorCodeValid = 1;
    event.Fields.Valid = 1;
    VpData->GuestVmcb.ControlArea.EventInj = event.AsUInt64;
}



UINT64 TranslateVirtualToPhysical(
    UINT64 PID,
    UINT64 VirtualAddress
) {
    auto dirbase = GetProcessCr3(PID);
    if (dirbase == 0) return 0;
    return TranslateLinearAddress(dirbase, VirtualAddress);
}

UINT64 MapPageVirtual(
    UINT64 pid_local,
    UINT64 virtaddr_local,
    UINT64 pid_remote,
    UINT64 virtaddr_remote,
    PSHARED_VIRTUAL_PROCESSOR_DATA SharedVpData
) {
    UINT64 dirbase_local, dirbase_remote;
    UINT64 phyaddr_local, phyaddr_remote;
    if ((dirbase_local = GetProcessCr3(pid_local)) == 0 || (dirbase_remote = GetProcessCr3(pid_remote)) == 0)
        return 0;
    phyaddr_local = TranslateLinearAddress(dirbase_local, virtaddr_local);
    phyaddr_remote = TranslateLinearAddress(dirbase_remote, virtaddr_remote);
    if (phyaddr_local == 0 || phyaddr_remote == 0)
        return 0;

    //Since @phyaddr_remote is treated as HPA but we want to pass GPA.
    //Get the HPA that is pointed by GPA in NPT.
    phyaddr_remote = SvGetMapping(phyaddr_remote, SharedVpData);
    SvMapPage(phyaddr_local, phyaddr_remote, SharedVpData);
    // return the original physical address so we can restore it later
    return phyaddr_local;
}

UINT64 GetEProcessByPid(
    UINT64 pid
) {
    PEPROCESS pEprocess = nullptr;
    PsLookupProcessByProcessId((HANDLE)pid, &pEprocess);
    return as_UINT64(pEprocess);
}

enum VMMCALL_CODE {
    INIT_SVM = 1,
    GET_CR3,
    VTOP,
    GET_EPROC,
    MAP_PAGE,
    UNMAP_PAGE,
};

_IRQL_requires_same_
VOID
SvHandleVmmcall(
    _Inout_ PVIRTUAL_PROCESSOR_DATA VpData,
    _Inout_ PGUEST_CONTEXT GuestContext
)
{
    UNREFERENCED_PARAMETER(GuestContext);

    SV_DEBUG_BREAK();

    UINT64 vmmcall_code = GuestContext->VpRegs->Rbx;
    UINT64 vmmcall_return = 0xDEADC0DE;

    if (GuestContext->VpRegs->Rax == SV_MAGIC_BOOBIES) {
        //do cool stuff here

        switch(vmmcall_code){
        case INIT_SVM:
            SvSetNPT(true, VpData->HostStackLayout.SharedVpData);
            vmmcall_return = 1;
            break;
        case GET_CR3:
            vmmcall_return = GetProcessCr3(GuestContext->VpRegs->Rcx);
            break;
        case VTOP:
            vmmcall_return = TranslateVirtualToPhysical(
                GuestContext->VpRegs->Rcx,
                GuestContext->VpRegs->Rdx
            );
            break;
        case GET_EPROC:
            vmmcall_return = GetEProcessByPid(
                GuestContext->VpRegs->Rcx
            );
            break;
        case MAP_PAGE:
            //return the physical address whose mapping was replaced
            //so we can restore the mapping once we are done
            vmmcall_return = MapPageVirtual(
                GuestContext->VpRegs->Rcx,
                GuestContext->VpRegs->Rdx,
                GuestContext->VpRegs->R8,
                GuestContext->VpRegs->R9,
                VpData->HostStackLayout.SharedVpData
            );
            break;
        case UNMAP_PAGE:
            //unitmap the page back
            SvMapPage(
                GuestContext->VpRegs->Rcx,
                GuestContext->VpRegs->Rcx,
                VpData->HostStackLayout.SharedVpData
            );
            vmmcall_return = 1;
            break;
        default:
            vmmcall_return = 0xDEADC0DE;
        }
        GuestContext->VpRegs->Rax = vmmcall_return;

        VpData->GuestVmcb.StateSaveArea.Rip = VpData->GuestVmcb.ControlArea.NRip;
    }
    else {
        SvInjectInvalidOpcodeException(VpData);
    }

    return;
}


_IRQL_requires_same_
VOID
SvHandleCpuid(
    _Inout_ PVIRTUAL_PROCESSOR_DATA VpData,
    _Inout_ PGUEST_CONTEXT GuestContext
)
{
    int registers[4];   // EAX, EBX, ECX, and EDX
    int leaf, subLeaf;
    SEGMENT_ATTRIBUTE attribute;

    //
    // Execute CPUID as requested.
    //
    leaf = static_cast<int>(GuestContext->VpRegs->Rax);
    subLeaf = static_cast<int>(GuestContext->VpRegs->Rcx);
    __cpuidex(registers, leaf, subLeaf);

    switch (leaf)
    {
    default:
        break;
    }

    //
    // Update guest's GPRs with results.
    //
    GuestContext->VpRegs->Rax = registers[0];
    GuestContext->VpRegs->Rbx = registers[1];
    GuestContext->VpRegs->Rcx = registers[2];
    GuestContext->VpRegs->Rdx = registers[3];

    //
    // Then, advance RIP to "complete" the instruction.
    //
    VpData->GuestVmcb.StateSaveArea.Rip = VpData->GuestVmcb.ControlArea.NRip;
}

_IRQL_requires_same_
VOID
SvHandleMsrAccess(
    _Inout_ PVIRTUAL_PROCESSOR_DATA VpData,
    _Inout_ PGUEST_CONTEXT GuestContext
)
{
    ULARGE_INTEGER value;
    UINT32 msr;
    BOOLEAN writeAccess;

    msr = GuestContext->VpRegs->Rcx & MAXUINT32;
    writeAccess = (VpData->GuestVmcb.ControlArea.ExitInfo1 != 0);

    if (msr == IA32_MSR_EFER)
    {
        if (!writeAccess) {
            // Guest is READING the EFER
            value.QuadPart = VpData->GuestVmcb.StateSaveArea.Efer;
            // Clear the SVME bit to hide hypervisor presence
            value.LowPart = value.LowPart & (~EFER_SVME);
            GuestContext->VpRegs->Rax = value.LowPart;
            GuestContext->VpRegs->Rdx = value.HighPart;
        }
        else {
            // Guest is WRITING the EFER
            value.LowPart = GuestContext->VpRegs->Rax & MAXUINT32;
            value.HighPart = GuestContext->VpRegs->Rdx & MAXUINT32;
            // Never let the guest clear the SVME bit
            value.QuadPart |= EFER_SVME;
            VpData->GuestVmcb.StateSaveArea.Efer = value.QuadPart;
        }
    }
    else if (msr == SVM_MSR_VM_HSAVE_PA) {
        if (!writeAccess) {
            // READ
            GuestContext->VpRegs->Rax = 0;
            GuestContext->VpRegs->Rdx = 0;
        }
        else {
            // discard write or inject GP..?
            goto InjectGP;
        }
    }
    else
    {
        NT_ASSERT(((msr > 0x00001fff) && (msr < 0xc0000000)) ||
            ((msr > 0xc0001fff) && (msr < 0xc0010000)) ||
            (msr > 0xc0011fff));

        goto InjectGP;
    }

    //
    // Then, advance RIP to "complete" the instruction.
    //
    VpData->GuestVmcb.StateSaveArea.Rip = VpData->GuestVmcb.ControlArea.NRip;
    return;

InjectGP:
    SvInjectGeneralProtectionException(VpData);
    return;
}


_IRQL_requires_same_
VOID
SvHandleVmInstruction(
    _Inout_ PVIRTUAL_PROCESSOR_DATA VpData,
    _Inout_ PGUEST_CONTEXT GuestContext
)
{
    UNREFERENCED_PARAMETER(GuestContext);

    SvInjectInvalidOpcodeException(VpData);
}
