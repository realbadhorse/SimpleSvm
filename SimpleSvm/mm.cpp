
#include "SimpleSvm.hpp"
#include "mm.hpp"
#include "npt.hpp"
#include "nt_utils.hpp"
#include <intrin.h>


/*!
    @brief      Allocates page aligned, zero filled contiguous physical memory.

    @details    This function allocates page aligned nonpaged pool where backed
                by contiguous physical pages. The allocated memory is zero
                filled and must be freed with SvFreeContiguousMemory. The
                allocated memory is executable.

    @param[in]  NumberOfBytes - A size of memory to allocate in byte.

    @result     A pointer to the allocated memory filled with zero; or NULL when
                there is insufficient memory to allocate requested size.
 */

_Post_writable_byte_size_(NumberOfBytes)
_Post_maybenull_
_Must_inspect_result_
PVOID
SvAllocateContiguousAlignedMemory(
    _In_ SIZE_T NumberOfBytes
)
{
    PVOID memory;
    PHYSICAL_ADDRESS boundary, lowest, highest;

    boundary.QuadPart = 0;

    lowest.QuadPart = 0;
    highest.QuadPart = -1;

    // Round the required size to next page boundry if not already aligned
    if ((SIZE_T)PAGE_ALIGN(NumberOfBytes) != NumberOfBytes) {
        NumberOfBytes = (SIZE_T)PAGE_ALIGN(NumberOfBytes) + PAGE_SIZE;
    }

#pragma prefast(disable : 30030, "No alternative API on Windows 7.")
    memory = MmAllocateContiguousMemorySpecifyCacheNode(NumberOfBytes,
        lowest,
        highest,
        boundary,
        MmCached,
        MM_ANY_NODE_OK);

    // just crash the thing if OOM, i dont care enough to handle this
    NT_ASSERT(memory);
    NT_ASSERT(PAGE_ALIGN(memory) == memory);

    RtlZeroMemory(memory, NumberOfBytes);
    return memory;
}


_Post_writable_byte_size_(NumberOfBytes)
_Post_maybenull_
_Must_inspect_result_
PVOID
SvAllocateContiguousAlignedMemoryHidden(
    _In_ SIZE_T NumberOfBytes,
    _In_ PSHARED_VIRTUAL_PROCESSOR_DATA SharedVpData
)
{
    PVOID memory;
    PHYSICAL_ADDRESS boundary, lowest, highest;
    PHYSICAL_ADDRESS memory_pa;
    UINT64 NumOfPages;

    NT_ASSERT(SharedVpData->NestedPageTable.Pml4Entries[0].Fields.Valid == 1);

    memory = SvAllocateContiguousAlignedMemory(NumberOfBytes);
    NumOfPages = NumberOfBytes / PAGE_SIZE;

    // Need to do it for every page in requested memory since
    // function only operates on single pages
    int i;
    PVOID curr_page;
    for (
        i = 0, curr_page = memory;
        i < NumOfPages;
        i++
     ) {
        //technically unnecessory to call GetPA every iteration since its contiguous but whatever
        memory_pa = MmGetPhysicalAddress(curr_page);
        SvHidePage(memory_pa.QuadPart, SharedVpData);
        curr_page = reinterpret_cast<PVOID>(
            reinterpret_cast<UINT64>(curr_page) + PAGE_SIZE
            ); //lmao
    }

    SvDebugPrint("Stealth memory at: 0x%llx\n", memory);
    return memory;
}

/*!
    @brief      Frees memory allocated by SvAllocateContiguousMemory.

    @param[in]  BaseAddress - The address returned by SvAllocateContiguousMemory.
 */
_IRQL_requires_max_(DISPATCH_LEVEL)
_IRQL_requires_same_
VOID
SvFreeContiguousAlignedMemory(
    _In_ PVOID BaseAddress
)
{
    MmFreeContiguousMemory(BaseAddress);
}
