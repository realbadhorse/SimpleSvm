#include "SimpleSvm.hpp"
#include "mm.hpp"
#include "npt.hpp"


//
// WARNING!!
// Even though we do enable the NPT flag in each of the VMCB
// It will not take effect till that particular vcpu encounters
// a vmexit and loads the new VMCB with the NPT flag on vmrun
//
void SvSetNPT(
    _In_ bool enable,
    _Inout_ PSHARED_VIRTUAL_PROCESSOR_DATA SharedVpData
) {
    NT_ASSERT(SharedVpData->VpDataList);
    NT_ASSERT(SharedVpData->VpDataListSize);
    for (ULONG i = 0; i < SharedVpData->VpDataListSize; i++) {
        PVIRTUAL_PROCESSOR_DATA VpData = SharedVpData->VpDataList[i];
        NT_ASSERT(VpData);
        NT_ASSERT(VpData->HostStackLayout.Reserved1 == MAXUINT64);

        if (enable) {
            VpData->GuestVmcb.ControlArea.NpEnable |= SVM_NP_ENABLE_NP_ENABLE;
        }
        else {
            VpData->GuestVmcb.ControlArea.NpEnable &= ~SVM_NP_ENABLE_NP_ENABLE;
        }
    }
}


void SvSmashNestedTablePD(
    _In_ UINT64 GuestPhysicalAddress,
    _Inout_ PSHARED_VIRTUAL_PROCESSOR_DATA SharedVpData
) {
    virt_addr_t gpa{};
    gpa.Value = GuestPhysicalAddress;

    auto NestedTables = &SharedVpData->NestedPageTable;
    // Only create a COPY which we will set back in one go to prevent race problems
    PDE pd_entry_copy = NestedTables->PdeEntries[gpa.Fields.pdpt_index][gpa.Fields.pd_index];
    NT_ASSERT(pd_entry_copy.as_dir.Fields.Valid == 1);


    if (pd_entry_copy.as_dir.Fields.LargePage == 0) {
        return;
    }
    pd_entry_copy.as_dir.Fields.LargePage = 0;


    // FIXME: cannot be Hidden because this causes infinite recursion
    PPTE pt_table = static_cast<PPTE>(SvAllocateContiguousAlignedMemory(PAGE_SIZE));
    PHYSICAL_ADDRESS pt_table_pa = MmGetPhysicalAddress(pt_table);
    NT_ASSERT(pt_table);
    pd_entry_copy.as_dir.Fields.PageFrameNumber = pt_table_pa.QuadPart >> PAGE_SHIFT;


    gpa.Fields.offset = 0;
    gpa.Fields.pt_index = 0;
    for (UINT64 pt_offs = 0; pt_offs < 512; pt_offs++) {
        // Atomicity does not matter in this loop since this is the new @pd_entry_copy is not yet
        // in effect
        // for each PTE, set the necessory bits and set the pfn to point to each consecutive
        // 4KB 'segment' in the 2MB range previously pointed by the GPA PD
        pt_table[pt_offs].AsUInt64 = 0;

        gpa.Fields.pt_index = pt_offs;

        pt_table[pt_offs].Fields.Valid = 1;
        pt_table[pt_offs].Fields.User = 1;
        pt_table[pt_offs].Fields.Write = 1;
        pt_table[pt_offs].Fields.PageFrameNumber = gpa.Value >> PAGE_SHIFT;
    }

    // pd_entry is now in effect and points to a PT dir now
    NestedTables->PdeEntries[gpa.Fields.pdpt_index][gpa.Fields.pd_index].as_dir.AsUInt64 = pd_entry_copy.as_dir.AsUInt64;
}

//
// Returns HPA for given GPA
// in 4KB granularity, even if it is a 2MB page
//
UINT64 SvGetMapping(
    _In_ UINT64 GuestPhysicalAddress,
    _In_ PSHARED_VIRTUAL_PROCESSOR_DATA SharedVpData
) {
    virt_addr_t gpa;
    PHYSICAL_ADDRESS tmp_pa;
    PPDE pd_entry;
    PPTE pt_entry;
    gpa.Value = GuestPhysicalAddress;

    if (gpa.Fields.pml4_index != 0) return 0;

    pd_entry = &SharedVpData->NestedPageTable.PdeEntries[gpa.Fields.pdpt_index][gpa.Fields.pd_index];
    if (pd_entry->as_dir.Fields.Valid == 0) return 0;

    if (pd_entry->as_dir.Fields.LargePage == 1) {
        //even if it is 2mb page, return 4kb aligned HPA for given GPA
        return (pd_entry->as_page.Fields.PageFrameNumber << (PAGE_SHIFT + 9)) + (gpa.Fields.pt_index << 12);
    }
    else {
        tmp_pa.QuadPart = pd_entry->as_dir.Fields.PageFrameNumber << PAGE_SHIFT;
        pt_entry = static_cast<PPTE>(MmGetVirtualForPhysical(tmp_pa));
        return pt_entry->Fields.PageFrameNumber << PAGE_SHIFT;
    }
}

//
// All accesses to GPA @GuestPhysicalAddressDst will be redirected to HPA @HostPhysicalAddressSrc
//
void SvMapPage(
    _In_ UINT64 GuestPhysicalAddressDst,
    _In_ UINT64 HostPhysicalAddressSrc,
    _Inout_ PSHARED_VIRTUAL_PROCESSOR_DATA SharedVpData
) {
    NT_ASSERT(GuestPhysicalAddressDst);
    NT_ASSERT(HostPhysicalAddressSrc);
    NT_ASSERT(SharedVpData);

    virt_addr_t gpa_target;
    PPDE pd_entry;
    PHYSICAL_ADDRESS pt_dir_pa;
    PPTE pt_dir;
    gpa_target.Value = GuestPhysicalAddressDst;

    SvSmashNestedTablePD(GuestPhysicalAddressDst, SharedVpData);

    pd_entry = &SharedVpData->NestedPageTable.PdeEntries[gpa_target.Fields.pdpt_index][gpa_target.Fields.pd_index];

    //
    // This is the page we allocated for the PT dir using @SvSmashNestedTablePD
    // Unfortunately we dont keep track of the PT dirs we allocate so we will have
    // To convert the pfn back to corrosponding VA
    //
    pt_dir_pa.QuadPart = pd_entry->as_dir.Fields.PageFrameNumber << PAGE_SHIFT; //is this natural aligned?
    pt_dir = static_cast<PPTE>(MmGetVirtualForPhysical(pt_dir_pa));
    NT_ASSERT(pt_dir);

    pt_dir[gpa_target.Fields.pt_index].Fields.PageFrameNumber = HostPhysicalAddressSrc >> PAGE_SHIFT;
    
}


void SvHidePage(
    _In_ UINT64 GuestPhysicalAddress,
    _Inout_ PSHARED_VIRTUAL_PROCESSOR_DATA SharedVpData
) {
    NT_ASSERT(GuestPhysicalAddress);
    NT_ASSERT(SharedVpData);

    //needs to be visible
    //allocate the page once, hide page is common for all 'hidden' memory
    //this is to save memory and prevent infinite recursion in @SvHidePage
    static UINT64 dummypage = reinterpret_cast<UINT64>(SvAllocateContiguousAlignedMemory(PAGE_SIZE));
    NT_ASSERT(dummypage);
    static PHYSICAL_ADDRESS dummypage_pa = MmGetPhysicalAddress((PVOID)dummypage);
    NT_ASSERT(dummypage_pa.QuadPart);
    
    SvMapPage(GuestPhysicalAddress, dummypage_pa.QuadPart, SharedVpData);
}



//
// This function is discarded because @MmGetVirtualForPhysical doesnt work for
// `DirBase` of processes. Due to this we cannot traverse DirBase using virtual addresses,
// physical addresses need to be used to traverse it instead. To do this we will need to
// modify the windows paging tables by ourself (or using API such as MmMapIoSpace) which
// are risky against AntiCheats since we would be editing the windows kernel page tables
// 
// A solution to this would be to create our own page tables which would not be monitored
// and map P to V to traverse the page tables.
// However since we have not yet implemented those
// we will offload this operation to non-root mode. This is because, pages mapped by request
// of non-root mode using NPT are transparent and stealthy. Using this, the user can traverse
// the page tables themselves
// 
// Replace the mapping for page backed by local with physical address of remote
// 
//void SvMapGVA(
//    UINT64 GuestVirtualAddressRemote,
//    UINT64 GuestPagingBaseRemotePA,
//    UINT64 GuestVirtualAddressLocal,
//    UINT64 GuestPagingBaseLocalPA,
//    PSHARED_VIRTUAL_PROCESSOR_DATA SharedVpData
//) {
//    NT_ASSERT(GuestVirtualAddressRemote);
//    NT_ASSERT(GuestVirtualAddressLocal);
//    NT_ASSERT(GuestPagingBaseRemotePA);
//    NT_ASSERT(GuestPagingBaseLocalPA);
//
//    PPML4E pml4_table_local;
//    PPML4E pml4_table_remote;
//
//    PHYSICAL_ADDRESS paging_base_local_pa;
//    PHYSICAL_ADDRESS paging_base_remote_pa;
//
//
//    auto SvGVAToGPA = [](PPML4E PML4BaseVA, UINT64 VirtualAddress) -> UINT64 {
//        PML4E pml4_entry;
//        PPDPE pdpt_base;
//        PDPE pdpt_entry;
//        PPDE pd_base;
//        PDE pd_entry;
//        PPTE pt_base;
//        PTE pt_entry;
//
//        PHYSICAL_ADDRESS tmp_pa;
//        virt_addr_t va;
//
//        va.Value = VirtualAddress;
//        pml4_entry = PML4BaseVA[va.Fields.pml4_index];
//        if (pml4_entry.Fields.Valid == 0) return 0;
//
//        tmp_pa.QuadPart = pml4_entry.Fields.PageFrameNumber << PAGE_SHIFT;
//        pdpt_base = static_cast<PPDPE>(MmGetVirtualForPhysical(tmp_pa));
//        if (pdpt_base == 0) return 0;
//
//        pdpt_entry = pdpt_base[va.Fields.pdpt_index];
//        if (pdpt_entry.as_dir.Fields.Valid == 0) return 0;
//
//        tmp_pa.QuadPart = pdpt_entry.as_dir.Fields.PageFrameNumber << PAGE_SHIFT;
//        pd_base = static_cast<PPDE>(MmGetVirtualForPhysical(tmp_pa));
//        if (pd_base == 0) return 0;
//
//        pd_entry = pd_base[va.Fields.pd_index];
//        if (pd_entry.as_dir.Fields.Valid == 0) return 0;
//
//        //It is a 2MB page
//        if (pd_entry.as_dir.Fields.LargePage == 1) {
//            return pd_entry.as_page.Fields.PageFrameNumber << (PAGE_SHIFT + 9);
//        }
//        else {
//            tmp_pa.QuadPart = pd_entry.as_dir.Fields.PageFrameNumber << PAGE_SHIFT;
//            pt_base = static_cast<PPTE>(MmGetVirtualForPhysical(tmp_pa));
//            if (pt_base == 0) return 0;
//            pt_entry = pt_base[va.Fields.pt_index];
//            if (pt_entry.Fields.Valid == 0) return 0;
//            return pt_entry.Fields.PageFrameNumber << PAGE_SHIFT;
//        }
//    };
//
//    paging_base_local_pa.QuadPart = GuestPagingBaseLocalPA;
//    paging_base_remote_pa.QuadPart = GuestPagingBaseRemotePA;
//
//    pml4_table_local = static_cast<PPML4E>(MmGetVirtualForPhysical(paging_base_local_pa));
//    pml4_table_remote = static_cast<PPML4E>(MmGetVirtualForPhysical(paging_base_remote_pa));
//
//    UINT64 gpa_local = SvGVAToGPA(pml4_table_local, GuestVirtualAddressLocal);
//    UINT64 gpa_remote = SvGVAToGPA(pml4_table_remote, GuestVirtualAddressRemote);
//    //
//    // But due to how our @SvMapPage is implemented, it assumes that @gpa_remote is
//    // in fact a HPA
//    // 
//    // Therefore it is possible that the @gpa_remote has already been remapped by
//    // the hv.
//    // Due to this, any accesses to @gpa_remote will not result in access
//    // to the (gpa) @gpa_remote and instead will result in access to (hpa) @gpa_remote
//    // This is because the assumed unit-mapping (hpa)_remote <-> (gpa)_remote no longer exists
//    // after the past remap.
//    //
//    // If we want @gpa_local to be mapped to (gpa) @gpa_remote then we will have to walk the
//    // NPT and get the mapping for the page which is actually backing (gpa) @gpa_remote (which
//    // may or may not be unit-mapped)
//    // This is done via the following @SvGetMapping call
//    //
//    // However this implementation has another flaw. If the mapping for (gpa) @gpa_remote changes
//    // after @gpa_local is mapped to @gpa_remote; @gpa_local's mapping is dangling and no longer
//    // points to @gpa_remote's page.
//    // This has not yet resolved
//    //
//    gpa_remote = SvGetMapping(gpa_remote, SharedVpData);
//    // please dont crash
//    NT_ASSERT(gpa_remote);
//    SvMapPage(gpa_local, gpa_remote, SharedVpData);
//}


/*!
    @brief      Build pass-through style page tables used in nested paging.

    @details    This function build page tables used in Nested Page Tables. The
                page tables are used to translate from a guest physical address
                to a system physical address and pointed by the NCr3 field of
                VMCB, like the traditional page tables are pointed by CR3.

                The nested page tables built in this function are set to
                translate a guest physical address to the same system physical
                address. For example, guest physical address 0x1000 is
                translated into system physical address 0x1000.

                In order to save memory to build nested page tables, 2MB large
                pages are used (as opposed to the standard pages that describe
                translation only for 4K granularity. Also, only up to 512 GB of
                translation is built. 1GB huge pages are not used due to VMware
                not supporting this feature.

    @param[out] SharedVpData - Out buffer to build nested page tables.
 */
_IRQL_requires_same_
VOID
SvBuildNestedPageTables(
    _Inout_ PSHARED_VIRTUAL_PROCESSOR_DATA SharedVpData
)
{
    ULONG64 pdpBasePa, pdeBasePa, translationPa;


    //
    // Build only one PML4 entry. This entry has subtables that control up to
    // 512GB physical memory. PFN points to a base physical address of the page
    // directory pointer table.
    //
    PNESTED_PAGE_TABLE NestedTables = &SharedVpData->NestedPageTable;

    pdpBasePa = MmGetPhysicalAddress(&NestedTables->PdpEntries).QuadPart;

    NestedTables->Pml4Entries[0].AsUInt64 = 0;
    NestedTables->Pml4Entries[0].Fields.PageFrameNumber = pdpBasePa >> PAGE_SHIFT;

    //
    // The US (User) bit of all nested page table entries to be translated
    // without #VMEXIT, as all guest accesses are treated as user accesses at
    // the nested level. Also, the RW (Write) bit of nested page table entries
    // that corresponds to guest page tables must be 1 since all guest page
    // table accesses are threated as write access. See "Nested versus Guest
    // Page Faults, Fault Ordering" for more details.
    //
    // Nested page tables built here set 1 to those bits for all entries, so
    // that all translation can complete without triggering #VMEXIT. This does
    // not lower security since security checks are done twice independently:
    // based on guest page tables, and nested page tables. See "Nested versus
    // Guest Page Faults, Fault Ordering" for more details.
    //
    NestedTables->Pml4Entries[0].Fields.Valid = 1;
    NestedTables->Pml4Entries[0].Fields.Write = 1;
    NestedTables->Pml4Entries[0].Fields.User = 1;

    // Mark all other entries in pml4 as invalid
    // TODO: Make sure this is acceptable.
    for (int pml4_offs = 1; pml4_offs < 512; pml4_offs++) {
        NestedTables->Pml4Entries[pml4_offs].AsUInt64 = 0;
    }

    //
    // One PML4 entry controls 512 page directory pointer entires.
    //
    for (ULONG64 pdp_offs = 0; pdp_offs < 512; pdp_offs++)
    {
        NestedTables->PdpEntries[pdp_offs].as_dir.AsUInt64 = 0;
        //
        // PFN points to a base physical address of the page directory table.
        //
        pdeBasePa = MmGetPhysicalAddress(&NestedTables->PdeEntries[pdp_offs][0]).QuadPart;

        NestedTables->PdpEntries[pdp_offs].as_dir.Fields.PageFrameNumber = pdeBasePa >> PAGE_SHIFT;
        NestedTables->PdpEntries[pdp_offs].as_dir.Fields.Valid = 1;
        NestedTables->PdpEntries[pdp_offs].as_dir.Fields.Write = 1;
        NestedTables->PdpEntries[pdp_offs].as_dir.Fields.User = 1;

        //
        // One page directory entry controls 512 page directory entries.
        //
        // We do not explicitly configure PAT in the NPT entry. The consequences
        // of this are: 1) pages whose PAT (Page Attribute Table) type is the
        // Write-Combining (WC) memory type could be treated as the
        // Write-Combining Plus (WC+) while it should be WC when the MTRR type is
        // either Write Protect (WP), Writethrough (WT) or Writeback (WB), and
        // 2) pages whose PAT type is Uncacheable Minus (UC-) could be treated
        // as Cache Disabled (CD) while it should be WC, when MTRR type is WC.
        //
        // While those are not desirable, this is acceptable given that 1) only
        // introduces additional cache snooping and associated performance
        // penalty, which would not be significant since WC+ still lets
        // processors combine multiple writes into one and avoid large
        // performance penalty due to frequent writes to memory without caching.
        // 2) might be worse but I have not seen MTRR ranges configured as WC
        // on testing, hence the unintentional UC- will just results in the same
        // effective memory type as what would be with UC.
        //
        // See "Memory Types" (7.4), for details of memory types,
        // "PAT-Register PA-Field Indexing", "Combining Guest and Host PAT Types",
        // and "Combining PAT and MTRR Types" for how the effective memory type
        // is determined based on Guest PAT type, Host PAT type, and the MTRR
        // type.
        //
        // The correct approach may be to look up the guest PTE and copy the
        // caching related bits (PAT, PCD, and PWT) when constructing NTP
        // entries for non RAM regions, so the combined PAT will always be the
        // same as the guest PAT type. This may be done when any issue manifests
        // with the current implementation.
        //
        for (ULONG64 pd_offs = 0; pd_offs < 512; pd_offs++)
        {
            //
            // PFN points to a base physical address of system physical address
            // to be translated from a guest physical address. Set the PS
            // (LargePage) bit to indicate that this is a large page and no
            // subtable exists.
            //
            NestedTables->PdeEntries[pdp_offs][pd_offs].as_page.AsUInt64 = 0;

            translationPa = (pdp_offs * 512) + pd_offs;
            NestedTables->PdeEntries[pdp_offs][pd_offs].as_page.Fields.PageFrameNumber = translationPa;
            NestedTables->PdeEntries[pdp_offs][pd_offs].as_page.Fields.Valid = 1;
            NestedTables->PdeEntries[pdp_offs][pd_offs].as_page.Fields.Write = 1;
            NestedTables->PdeEntries[pdp_offs][pd_offs].as_page.Fields.User = 1;
            NestedTables->PdeEntries[pdp_offs][pd_offs].as_page.Fields.LargePage = 1;
        }
    }

}
