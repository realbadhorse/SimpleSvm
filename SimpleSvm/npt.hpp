#pragma once
#include "SimpleSvm.hpp"



void SvSetNPT(bool enable, PSHARED_VIRTUAL_PROCESSOR_DATA SharedVpData);

void SvSmashNestedTablePD(UINT64 GuestPhysicalAddress, PSHARED_VIRTUAL_PROCESSOR_DATA SharedVpData);

UINT64 SvGetMapping(UINT64 GuestPhysicalAddress, PSHARED_VIRTUAL_PROCESSOR_DATA SharedVpData);

void SvMapPage(UINT64 GuestPhysicalAddressDst, UINT64 HostPhysicalAddressSrc, PSHARED_VIRTUAL_PROCESSOR_DATA SharedVpData);

void SvHidePage(UINT64 GuestPhysicalAddress, PSHARED_VIRTUAL_PROCESSOR_DATA SharedVpData);

void SvBuildNestedPageTables(PSHARED_VIRTUAL_PROCESSOR_DATA SharedVpData);

UINT64 SvGetMapping(
    _In_ UINT64 GuestPhysicalAddress,
    _In_ PSHARED_VIRTUAL_PROCESSOR_DATA SharedVpData
);