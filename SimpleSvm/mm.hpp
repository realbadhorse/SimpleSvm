#pragma once

#include <ntifs.h>


_Post_writable_byte_size_(NumberOfBytes)
_Post_maybenull_
_Must_inspect_result_
PVOID SvAllocateContiguousAlignedMemoryHidden(SIZE_T NumberOfBytes, PSHARED_VIRTUAL_PROCESSOR_DATA SharedVpData);

_IRQL_requires_max_(DISPATCH_LEVEL)
_IRQL_requires_same_
PVOID SvAllocateContiguousAlignedMemory(SIZE_T NumberOfBytes);

VOID
SvFreeContiguousAlignedMemory(_In_ PVOID BaseAddress);


