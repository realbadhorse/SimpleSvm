#pragma once


ULONG_PTR GetProcessCr3(UINT64 ProcessID);

ULONG_PTR GetKernelDirBase();

UINT64 TranslateLinearAddress(UINT64 directoryTableBase, UINT64 virtualAddress);

PHYSICAL_ADDRESS UIntToPhysical(UINT64 val);
