#include <ntifs.h>  
#include "nt_utils.hpp"

static constexpr UINT64 PMASK = (~0xfull << 8) & 0xfffffffffull;

//https://ntdiff.github.io/
#define WINDOWS_1803 17134
#define WINDOWS_1809 17763
#define WINDOWS_1903 18362
#define WINDOWS_1909 18363
#define WINDOWS_2004 19041
#define WINDOWS_20H2 19569
#define WINDOWS_21H1 20180

UINT64 GetUserDirectoryTableBaseOffset()
{
    RTL_OSVERSIONINFOW ver = { 0 };
    RtlGetVersion(&ver);

    switch (ver.dwBuildNumber)
    {
    case WINDOWS_1803:
        return 0x0278;
        break;
    case WINDOWS_1809:
        return 0x0278;
        break;
    case WINDOWS_1903:
        return 0x0280;
        break;
    case WINDOWS_1909:
        return 0x0280;
        break;
    case WINDOWS_2004:
        return 0x0388;
        break;
    case WINDOWS_20H2:
        return 0x0388;
        break;
    case WINDOWS_21H1:
        return 0x0388;
        break;
    default:
        return 0x0388;
    }
}

//check normal dirbase if 0 then get from UserDirectoryTableBas
ULONG_PTR GetProcessCr3(UINT64 ProcessID)
{
    PEPROCESS pProcess = NULL;
    if (ProcessID == 0) return 0;

    NTSTATUS NtRet = PsLookupProcessByProcessId((HANDLE)ProcessID, &pProcess);
    if (NtRet != STATUS_SUCCESS) return 0;
    PUCHAR process = (PUCHAR)pProcess;

    ULONG_PTR process_dirbase = *(PULONG_PTR)(process + 0x28); //dirbase x64, 32bit is 0x18
    if (process_dirbase == 0)
    {
        UINT64 UserDirOffset = GetUserDirectoryTableBaseOffset();
        process_dirbase = *(PULONG_PTR)(process + UserDirOffset);
    }
    return process_dirbase;
}

//Reading CR3 might be incorrect if running in user thread context
ULONG_PTR GetKernelDirBase()
{
    PUCHAR process = (PUCHAR)PsGetCurrentProcess();
    ULONG_PTR cr3 = *(PULONG_PTR)(process + 0x28); //dirbase x64, 32bit is 0x18
    return cr3;
}


NTSTATUS ReadPhysicalAddress(PVOID TargetAddress, PVOID lpBuffer, SIZE_T Size, SIZE_T* BytesRead)
{
    MM_COPY_ADDRESS AddrToRead = { 0 };
    AddrToRead.PhysicalAddress.QuadPart = (UINT64)TargetAddress;
    return MmCopyMemory(lpBuffer, AddrToRead, Size, MM_COPY_MEMORY_PHYSICAL, BytesRead);
}


// Physical address @directoryTableBase
UINT64 TranslateLinearAddress(UINT64 directoryTableBase, UINT64 virtualAddress) {
    directoryTableBase &= ~0xf;

    UINT64 pageOffset = virtualAddress & ~(~0ul << PAGE_SHIFT);
    UINT64 pte = ((virtualAddress >> 12) & (0x1ffll));
    UINT64 pt = ((virtualAddress >> 21) & (0x1ffll));
    UINT64 pd = ((virtualAddress >> 30) & (0x1ffll));
    UINT64 pdp = ((virtualAddress >> 39) & (0x1ffll));

    SIZE_T readsize = 0;
    UINT64 pdpe = 0;
    ReadPhysicalAddress((PVOID)(directoryTableBase + 8 * pdp), &pdpe, sizeof(pdpe), &readsize);
    if (~pdpe & 1)
        return 0;

    UINT64 pde = 0;
    ReadPhysicalAddress((PVOID)((pdpe & PMASK) + 8 * pd), &pde, sizeof(pde), &readsize);
    if (~pde & 1)
        return 0;

    /* 1GB large page, use pde's 12-34 bits */
    if (pde & 0x80)
        return (pde & (~0ull << 42 >> 12)) + (virtualAddress & ~(~0ull << 30));

    UINT64 pteAddr = 0;
    ReadPhysicalAddress((PVOID)((pde & PMASK) + 8 * pt), &pteAddr, sizeof(pteAddr), &readsize);
    if (~pteAddr & 1)
        return 0;

    /* 2MB large page */
    if (pteAddr & 0x80)
        return (pteAddr & PMASK) + (virtualAddress & ~(~0ull << 21));

    virtualAddress = 0;
    ReadPhysicalAddress((PVOID)((pteAddr & PMASK) + 8 * pte), &virtualAddress, sizeof(virtualAddress), &readsize);
    virtualAddress &= PMASK;

    if (!virtualAddress)
        return 0;

    return virtualAddress + pageOffset;
}


PHYSICAL_ADDRESS UIntToPhysical(UINT64 val) {
    PHYSICAL_ADDRESS phy;
    phy.QuadPart = val;
    return phy;
}